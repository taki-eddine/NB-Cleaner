package org.nb.cleaner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.spi.editor.document.OnSaveTask;
import org.openide.util.lookup.ServiceProvider;

/**
 * @brief This class will clean the text: trim trailing whitespace, ensure new line at EOF
 *        and remove double new line.
**/
@ServiceProvider(service = TextCleaner.class)
public class TextCleaner implements OnSaveTask
{  // ==== [ Private ] =========================================================
   // Data members ------------------------
   private Document _doc = null;

   // Methods -----------------------------
   private String _getDocumentText ()
   {  try
      {  return _doc.getText(0, _doc.getLength());  }
      catch (BadLocationException except)
      {  except.printStackTrace();
         return null;
      }
   }

   private void _removeDocumentText (int offset, int length)
   {  try
      {  _doc.remove(offset, length);  }
      catch (BadLocationException except)
      {  except.printStackTrace();  }
   }

   private void _insertDocumentText (String str, int offset)
   {  try
      {  _doc.insertString(offset, str, null);  }
      catch (BadLocationException except)
      {  except.printStackTrace();  }
   }

   private void _replaceDocumentText (String find, String replace)
   {  String  doc_text    = _getDocumentText();
      Pattern pattern     = Pattern.compile(find);
      Matcher matcher     = pattern.matcher(doc_text);
      int     match_len   = 0,
              diff_offset = 0;  // To optimize replacing, instead of update the text
                                // and launch a regex command, each time we change the text,
                                // we count the number of difference, so after remove a subtext,
                                // the next position will be: regex_match_position - difference.

      while ( matcher.find() )
      {  match_len = matcher.end() - matcher.start();
         _removeDocumentText(matcher.start() - diff_offset, match_len);
         _insertDocumentText(replace, matcher.start() - diff_offset);
         diff_offset += Math.abs(match_len - replace.length());
      }
   }

   // ==== [ Public ] ==========================================================
   // Constructors ------------------------
   public TextCleaner ()
   {  this(null);  }

   public TextCleaner (Document doc)
   {  _doc = doc;  }

   // Methods -----------------------------
   @Override
   public void performTask ()
   {  try
      {  // - trim trailling whitespaces - //
         _replaceDocumentText("[ \t]+(?=\\n|\\Z)", "");
         // - remove doubled new line - //
         _replaceDocumentText("\\A\\n+|\\n+\\Z", "");  // at the beginning and te end.
         _replaceDocumentText("(?<=\\n)\\n+", "\n");   // at the middle.

         // - ensure new line at EOF - //
         _replaceDocumentText("(?<=[^\\n])\\z", "\n");
      }
      catch (Exception except)
      {  except.printStackTrace();  }
   }

   @Override
   public void runLocked (Runnable runnable)
   {  runnable.run();  }

   @Override
   public boolean cancel ()
   {  return true;  }
}
