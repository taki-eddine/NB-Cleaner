package org.nb.cleaner;

import org.netbeans.spi.editor.document.OnSaveTask;
import org.netbeans.spi.editor.document.OnSaveTask.Factory;
import org.netbeans.spi.editor.document.OnSaveTask.Context;
import org.netbeans.api.editor.mimelookup.MimeRegistration;

/**
 * @brief Provide our service to Netbeans.
**/
@MimeRegistration(mimeType = "", service = OnSaveTask.Factory.class, position = 1500)
public class TextCleanerFactoryProvider implements Factory
{  @Override
   public OnSaveTask createTask (Context context)
   {  return new TextCleaner(context.getDocument());  }
}
